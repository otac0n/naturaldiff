﻿using System;
using System.Collections.Generic;
using System.Linq;
using NaturalDiffEngine;

namespace NaturalDiff
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var file1 = System.IO.File.ReadAllLines("file1.txt");
            var file2 = System.IO.File.ReadAllLines("file2.txt");

            var diffMatrix = LevenshtienDistance.GetLevenshtienDistances(file1, file2);
            var minPath = LevenshtienDistance.FindShortestPaths(diffMatrix);
            var bestRoutes = minPath.GetRoutes().AllMinBy(route => ScoreRoute(route, file1, file2), new ScoreComparer());

            foreach (var route in bestRoutes)
            {
                RenderRouteInDiffMatrix(diffMatrix, route);
                RenderRoute(route);
                RenderRouteDiff(route, file1, file2);
            }

            Console.ReadKey(true);
        }

        private class ScoreComparer : IComparer<Tuple<int[], int[]>>
        {
            public int Compare(Tuple<int[], int[]> a, Tuple<int[], int[]> b)
            {
                Func<int[], int[]> sort = items => items.OrderByDescending(i => i).ToArray();

                return CompareExtensions.Combine(
                    () => CompareExtensions.CompareLists(sort(a.Item2), sort(b.Item2)),
                    () => CompareExtensions.CompareLists(sort(a.Item1), sort(b.Item1)));
            }
        }

        private static Tuple<int[], int[]> ScoreRoute(Chain<Delta> route, string[] file1, string[] file2)
        {
            Func<Func<Delta, int>, int[]> chunk = selector =>
            {
                return route.ChunkBy(selector).Where(g => g.Key != 0).Select(c => c.Count()).ToArray();
            };

            Func<char, int> weigh = c =>
            {
                switch (c)
                {
                    case '(':
                    case '{':
                    case '[':
                        return -1;

                    case ')':
                    case '}':
                    case ']':
                        return 1;

                    default:
                        return 0;
                }
            };

            Func<int[], string[], int[]> imbalance = (chunks, file) =>
            {
                var results = new List<int>();

                var l = 0;
                foreach (var size in chunks)
                {
                    var balance = 0;
                    var maxImbalance = 0;

                    for (int i = size; i > 0; i--, l++)
                    {
                        var line = file[l];
                        foreach (var c in line)
                        {
                            balance += weigh(c);
                            maxImbalance = Math.Max(maxImbalance, balance);
                        }
                    }

                    results.Add(maxImbalance);
                }

                return results.ToArray();
            };

            return Tuple.Create(
                imbalance(chunk(r => r.X), file1),
                imbalance(chunk(r => r.Y), file2));
        }

        private static void RenderRoute(Chain<Delta> route)
        {
            Console.WriteLine(string.Concat(route.Select(r =>
                r == Delta.Right ? "-" :
                r == Delta.Down ? "+" :
                r == Delta.DownRight ? "=" :
                "?")));
            Console.WriteLine();
        }

        private static void RenderRouteInDiffMatrix(int[,] diffMatrix, Chain<Delta> route)
        {
            int m = diffMatrix.GetLength(0) - 1;
            int n = diffMatrix.GetLength(1) - 1;

            int u = -1;
            int v = -1;

            var delta = route.Value;
            route = route.Tail;

            u += delta.X;
            v += delta.Y;

            for (int j = 0; j <= n; j++)
            {
                for (int i = 0; i <= m; i++)
                {
                    if (i == u && j == v)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        if (route.Length > 0)
                        {
                            delta = route.Value;
                            route = route.Tail;
                            u += delta.X;
                            v += delta.Y;
                        }
                        else
                        {
                            if (u < m) u++;
                            if (v < n) v++;
                        }
                    }

                    Console.Write(diffMatrix[i, j].ToString().PadLeft(2).PadRight(3));
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }

        private static void RenderRouteDiff(Chain<Delta> minPath, string[] file1, string[] file2)
        {
            int u = -1;
            int v = -1;

            foreach (var delta in minPath)
            {
                u += delta.X;
                v += delta.Y;

                var left = file1[u];
                var right = file2[v];

                if (delta.Y == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("-{0}", left);
                }
                else if (delta.X == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("+{0}", right);
                }
                else
                {
                    if (left != right)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("-{0}", left);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("+{0}", right);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine(" {0}", left);
                    }
                }
            }
        }
    }
}
