﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NaturalDiff
{
    internal static class EnumerableExtensions
    {
        public static IEnumerable<IGrouping<TKey, TValue>> ChunkBy<TKey, TValue>(this IEnumerable<TValue> source, Func<TValue, TKey> keySelector, IEqualityComparer<TKey> comparer = null)
        {
            comparer = comparer ?? EqualityComparer<TKey>.Default;

            Chunk<TKey, TValue> chunk = null;
            foreach (var item in source)
            {
                var key = keySelector(item);
                if (chunk != null && !comparer.Equals(chunk.Key, key))
                {
                    yield return chunk;
                    chunk = null;
                }

                if (chunk == null)
                {
                    chunk = new Chunk<TKey, TValue>(key);
                }

                chunk.Add(item);
            }

            if (chunk != null)
            {
                yield return chunk;
                chunk = null;
            }
        }

        public static List<T> AllMinBy<T, TCompare>(this IEnumerable<T> items, Func<T, TCompare> valueSelector, IComparer<TCompare> comparer = null)
        {
            comparer = comparer ?? Comparer<TCompare>.Default;

            var first = true;
            var min = default(TCompare);
            var minItems = new List<T>();

            foreach (var item in items)
            {
                var value = valueSelector(item);
                var comp = first ? -1 : comparer.Compare(value, min);
                first = false;

                if (comp < 0)
                {
                    min = value;
                    minItems.Clear();
                }

                if (comp <= 0)
                {
                    minItems.Add(item);
                }
            }

            return minItems;
        }

        public static IEnumerable<Tuple<TFirst, TSecond>> Zip<TFirst, TSecond>(this IEnumerable<TFirst> first, IEnumerable<TSecond> second)
        {
            return first.Zip(second, (a, b) => Tuple.Create(a, b));
        }

        private class Chunk<TKey, TValue> : List<TValue>, IGrouping<TKey, TValue>
        {
            public Chunk(TKey key)
            {
                this.Key = key;
            }

            public TKey Key { get; private set; }
        }
    }
}
