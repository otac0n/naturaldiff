﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NaturalDiff
{
    internal class CompareExtensions
    {
        public static int CheckNulls<T>(T a, T b, Func<int> rest) where T : class
        {
            return CheckNulls(a, b, (_a, _b) => rest());
        }

        public static int CheckNulls<T>(T a, T b, Func<T, T, int> rest) where T : class
        {
            if (a == null)
            {
                return b == null ? 0 : -1;
            }
            else if (b == null)
            {
                return 1;
            }

            return rest(a, b);
        }

        public static int Combine(IEnumerable<int> comparisons)
        {
            return comparisons.Where(c => c != 0).FirstOrDefault();
        }

        public static int Combine(params Func<int>[] comparisons)
        {
            return Combine(comparisons.Select(c => c()));
        }

        public static int CompareLists<T>(IList<T> a, IList<T> b, IComparer<T> comparer = null)
        {
            comparer = comparer ?? Comparer<T>.Default;
            return CheckNulls(a, b, () =>
                Combine(
                    () => Combine(a.Zip(b, comparer.Compare)),
                    () => a.Count.CompareTo(b.Count)));
        }
    }
}
