﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NaturalDiff
{
    public class Subarray<T>
    {
        public Subarray(T[] array, int offset, int count)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            if (offset < 0 || array.Length < offset + count)
            {
                throw new IndexOutOfRangeException();
            }

            this.Array = array;
            this.Offset = offset;
            this.Length = count;
        }

        public int Offset
        {
            get;
            private set;
        }

        public int Length
        {
            get;
            private set;
        }

        public T[] Array
        {
            get;
            private set;
        }

        public T this[int i]
        {
            get
            {
                if(i < 0 || i >= this.Length)
                {
                    throw new IndexOutOfRangeException();
                }

                return this.Array[i + this.Offset];
            }

            set
            {
                if (i < 0 || i >= this.Length)
                {
                    throw new IndexOutOfRangeException();
                }

                this.Array[i + this.Offset] = value;
            }
        }
    }
}
