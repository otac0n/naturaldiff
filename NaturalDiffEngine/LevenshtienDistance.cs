﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NaturalDiffEngine
{
    public static class LevenshtienDistance
    {
        public static int Calculate<T>(IEnumerable<T> data1, IEnumerable<T> data2) where T : IComparable<T>
        {
            return Calculate(
                data1.ToArray(),
                data2.ToArray());
        }

        public static int Calculate<T>(T[] data1, T[] data2) where T : IComparable<T>
        {
            var m = data1.Length;
            var n = data2.Length;
            var d = new int[m + 1, n + 1];

            for (int i = 0; i <= m; i++)
            {
                d[i, 0] = i;
            }

            for (int j = 0; j <= n; j++)
            {
                d[0, j] = j;
            }

            for (int j = 1; j <= n; j++)
            {
                for (int i = 1; i <= m; i++)
                {
                    if (data1[i - 1].CompareTo(data2[j - 1]) == 0)
                    {
                        d[i, j] = d[i - 1, j - 1];
                    }
                    else
                    {
                        d[i, j] = Math.Min(
                            d[i - 1, j] + 1,
                            d[i, j - 1] + 1);
                    }
                }
            }

            return d[m, n];
        }

        public static int[,] GetLevenshtienDistances(string[] a, string[] b)
        {
            var m = a.Length;
            var n = b.Length;
            var diffMatrix = new int[m + 1, n + 1];

            for (int i = 0; i < m; i++)
            {
                diffMatrix[i, n] = a[i].Length + 1;
            }

            for (int j = 0; j < n; j++)
            {
                diffMatrix[m, j] = b[j].Length + 1;
            }

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    diffMatrix[i, j] = LevenshtienDistance.Calculate(a[i], b[j]);
                }
            }

            return diffMatrix;
        }

        public static Path FindShortestPaths(int[,] diffMatrix)
        {
            int m = diffMatrix.GetLength(0) - 1;
            int n = diffMatrix.GetLength(1) - 1;

            var minCostPaths = new Dictionary<Tuple<int, int>, Path>();

            Func<Tuple<int, int>, Path> lookup = null;
            lookup = point =>
            {
                var x = point.Item1;
                var y = point.Item2;
                var rightEdge = x == m;
                var bottomEdge = y == n;

                if (rightEdge && bottomEdge)
                {
                    return new Path(0);
                }

                Path value;
                if (!minCostPaths.TryGetValue(point, out value))
                {
                    if (bottomEdge)
                    {
                        var right = lookup(Tuple.Create(x + 1, y));
                        value = new Path(
                            right.Value + diffMatrix[x, n],
                            new KeyValuePair<Delta, Path>(Delta.Right, right));
                    }
                    else if (rightEdge)
                    {
                        var down = lookup(Tuple.Create(x, y + 1));
                        value = new Path(
                            down.Value + diffMatrix[m, y],
                            new KeyValuePair<Delta, Path>(Delta.Down, down));
                    }
                    else
                    {
                        var rght = lookup(Tuple.Create(x + 1, y));
                        var down = lookup(Tuple.Create(x, y + 1));
                        var diag = lookup(Tuple.Create(x + 1, y + 1));

                        var rghtValue = rght.Value + diffMatrix[x, n];
                        var downValue = down.Value + diffMatrix[m, y];
                        var diagValue = diag.Value + diffMatrix[x, y];

                        var newValue = Math.Min(Math.Min(rghtValue, downValue), diagValue);

                        var minPaths = new List<KeyValuePair<Delta, Path>>();
                        if (diagValue == newValue)
                        {
                            minPaths.Add(new KeyValuePair<Delta, Path>(Delta.DownRight, diag));
                        }

                        if (downValue == newValue)
                        {
                            minPaths.Add(new KeyValuePair<Delta, Path>(Delta.Down, down));
                        }

                        if (rghtValue == newValue)
                        {
                            minPaths.Add(new KeyValuePair<Delta, Path>(Delta.Right, rght));
                        }

                        value = new Path(newValue, minPaths.ToArray());
                    }

                    minCostPaths.Add(point, value);
                }

                return value;
            };

            var minPath = lookup(Tuple.Create(0, 0));

            return minPath;
        }
    }
}
