﻿namespace NaturalDiffEngine
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class Chain<T> : ICollection<T>
    {
        private static readonly Chain<T> Empty = new Chain<T>();

        public Chain(T value, Chain<T> tail = null)
        {
            tail = tail ?? Empty;
            this.Value = value;
            this.Tail = tail;
            this.Length = tail.Length + 1;
        }

        private Chain()
        {
            this.Length = 0;
            this.Tail = this;
        }

        public T Value { get; private set; }

        public Chain<T> Tail { get; private set; }

        public int Length { get; private set; }

        public IEnumerator<T> GetEnumerator()
        {
            var next = this;
            while (next.Length > 0)
            {
                yield return next.Value;
                next = next.Tail;
            }
        }

        void ICollection<T>.Add(T item)
        {
            throw new NotSupportedException();
        }

        void ICollection<T>.Clear()
        {
            throw new NotSupportedException();
        }

        bool ICollection<T>.Contains(T item)
        {
            var comparer = EqualityComparer<T>.Default;
            return this.Any(i => comparer.Equals(i, item));
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("arrayIndex");
            }
            else if (this.Length + arrayIndex > array.Length)
            {
                throw new ArgumentException("arrayIndex");
            }

            foreach (var item in this)
            {
                array[arrayIndex++] = item;
            }
        }

        int ICollection<T>.Count
        {
            get { return this.Length; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return true; }
        }

        bool ICollection<T>.Remove(T item)
        {
            throw new NotSupportedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
