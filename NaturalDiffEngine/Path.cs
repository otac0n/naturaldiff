﻿namespace NaturalDiffEngine
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    public class Path
    {
        private Chain<Delta>[] deltas = null;

        public Path(int value, params KeyValuePair<Delta, Path>[] tails)
        {
            this.Value = value;
            this.Tails = tails.ToDictionary(t => t.Key, t => t.Value);
        }

        public int Value { get; private set; }

        public Dictionary<Delta, Path> Tails { get; private set; }

        public Chain<Delta>[] GetRoutes()
        {
            var deltas = this.deltas;
            if (deltas == null)
            {
                if (this.Tails.Count == 0)
                {
                    deltas = (Chain<Delta>[])Enumerable.Empty<Chain<Delta>>();
                }
                else
                {
                    var deltasList = new List<Chain<Delta>>();

                    foreach (var d in this.Tails.Keys)
                    {
                        var routes = this.Tails[d].GetRoutes();
                        if (routes.Length == 0)
                        {
                            deltasList.Add(new Chain<Delta>(d));
                        }
                        else
                        {
                            deltasList.AddRange(routes.Select(r => new Chain<Delta>(d, r)));
                        }
                    }

                    deltas = deltasList.ToArray();
                }

                Interlocked.CompareExchange(ref this.deltas, deltas, null);
            }

            return this.deltas;
        }
    }
}
