﻿namespace NaturalDiffEngine
{
    using System.Diagnostics;

    [DebuggerDisplay("{X}, {Y}")]
    public class Delta
    {
        public static readonly Delta Right = new Delta(1, 0);
        public static readonly Delta Down = new Delta(0, 1);
        public static readonly Delta DownRight = new Delta(1, 1);

        private Delta(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public int X { get; private set; }

        public int Y { get; private set; }
    }
}
